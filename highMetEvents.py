#!/usr/bin/env python2
from __future__ import division
import glob, re, sys, os, math
import numpy as np
nb2fb = 1.e6
import cPickle as pickle

from ROOT import *

def GetXsecWeight(event,sample,applyMCwgt,nEvt_wgt,DSID):
    totWgt = 1e99
    if (applyMCwgt is False):
       totWgt = event.Nevt
    else:
       totWgt = nEvt_wgt
  
    #if event.pu_weight > 1e4 or event.pu_weight < -1e4:
    #    print event.xsec,event.geneff,totWgt
 
    if(event.xsec>0):
       return event.xsec*nb2fb * event.geneff / totWgt      # Scaling to 1ifb
    elif(DSID=="410646"):
       return 37.936*nb2fb * event.geneff / totWgt
    elif(DSID=="410647"):
       return 37.905*nb2fb * event.geneff / totWgt
    elif(DSID=="410648"):
       return 3.997*nb2fb * event.geneff / totWgt
    elif(DSID=="410649"):
       return 3.9939*nb2fb * event.geneff / totWgt
    else:
       print "ERROR: event.xsec=%g for sample=%s" % (event.xsec,sample)
       print "        quitting!"
       raise BaseException("Negative weight")
    return 1

def GetLeptonSfWeight(event,channel):
    SF = 1.
    # if(db.args.doFakes=="1" and "ee" in channel):
       # if(event.pass_only_preor_ee==1): SF = event.el_preor_SF[0]*event.el_preor_SF[1]
    if 0: pass # replace fakes call
    elif("ee" in channel and event.pass_ee==1): SF = event.el_SF[0]*event.el_SF[1]
    if("uu" in channel and event.pass_uu==1): SF = event.mu_SF[0]*event.mu_SF[1]
    if("eu" in channel and event.pass_eu==1): SF = event.el_SF[0]*event.mu_SF[0]
    if(SF<=0):
       #print "WARNING (GetLeptonSfWeight): SF=0 in channel=",channel
       SF = 1
    return SF

def GetJetSfWeight(event,channel):
    SF = 1.
    if("0b" in channel or "1b" in channel or "2b" in channel or "vbf" in channel): SF = event.btag_signal_jets_SF
    if(SF<=0):
       #print "WARNING (GetJetSfWeight): SF=0 in channel=",channel
       SF = 1
    return SF

def GetTriggerSfWeight(event,channel):
    SF = 1.
    # if(db.args.doFakes=="1" and "ee" in channel):
    #     if(event.pass_only_preor_ee==1): SF = event.trig_ee_SF
    if 0: pass # replace fakes call
    elif("ee" in channel and event.pass_ee==1): SF = event.trig_ee_SF
    if("uu" in channel and event.pass_uu==1): SF = event.trig_uu_SF
    if("eu" in channel and event.pass_eu==1): SF = event.trig_eu_SF
    if(SF<=0):
       #print "WARNING (GetTriggerSfWeight): SF=0 in channel=",channel
       SF = 1.
    return SF

def GetWeight(event,channel,sample,applyMCwgt,nEvt_wgt,DSID):
    if (sample == "data"): return 1
   
    wgt_mc = event.mconly_weight
    wgt_pu = event.pu_weight/event.mconly_weight  ## pu_weight is multiplied by mconly_weight in ST :S
    wgt_kf = event.kF_weight
    try:
        wgt_xs = GetXsecWeight(event,sample,applyMCwgt,nEvt_wgt,DSID)
    except:
        raise BaseException("Bad weight")
    wgt_lsf = GetLeptonSfWeight(event,channel)
    wgt_jsf = GetJetSfWeight(event,channel)
    wgt_tsf = GetTriggerSfWeight(event,channel)
   
    #if event.pu_weight > 1e4 or event.pu_weight < -1e4:
    #    print wgt_pu,event.pu_weight,event.mconly_weight,event.mconly_weights[0]
   
    wgt = 1.
    if applyMCwgt:
       wgt *= wgt_mc
       wgt *= wgt_pu
    wgt *= wgt_kf
    wgt *= wgt_xs
    wgt *= wgt_lsf
    #wgt *= wgt_jsf
    wgt *= wgt_tsf
   
    return wgt

def makeHist(iPath,oDir):
    """ Make histogram for one file """
    print "#"*50
    print iPath
    print i,"/",len(glob.glob(iPaths))
    print "-"*50
    # check if directory is empty
    if len(glob.glob(iPath+"/data-tinytrees/*root"))!=1:
        print iPath+"/data-tinytrees/*root"
        print "SKIPPING, wrong number"
        return
    isMc = "mc16_13TeV" in iPath
    if isMc: name = ".".join(iPath.split("/")[-2:])
    else: name = os.path.dirname(iPath).split("/")[-1]
    # print name; quit()
    oPath = os.path.join(oDir,name)
    oPath+=".pickle"
    # print oPath; quit()
    if isMc:
        iPathHist = glob.glob(iPath+"/hist-user.*root")[0]
        # load cutflow
        histFile = TFile(iPathHist,"READ")
        cutflow = histFile.Get("cutflow_wgt_ee_pstOR")
        nEvt_wgt = cutflow.GetBinContent(1)
        DSID=re.compile("\d\d\d\d\d\d").findall(iPath)[0]
        sample = "mc"
    else:
        cutflow = 1
        nEvt_wgt = 1
        # iPathTree = glob.glob(iPath+"/*root")[0]
        DSID=1
        sample = "data"
    iPathTree = glob.glob(iPath+"/data-tinytrees/*root")[0]
    print "Loading root file:",iPathTree
    print "Loading name:","nominal"
    f = TFile.Open(iPathTree)
    f.Print()
    print "-"*50
    try: f.GetName()
    except: print "ERROR opening file"
    events = f.Get("nominal")
    try: events.GetName()
    except: print "ERROR loading tree"

    passed=0
    c=0
    output = []
    for event in events:
        # print "Running event",c
        applyMCwgt =  "mc16_13TeV" in iPath
        channel = "uu"

        try: weight = GetWeight(event,channel,sample,applyMCwgt,nEvt_wgt,DSID)
        except: 
            print "Warning: skipping event"
            return

        c+=1
        met = event.met_wmuon_cst_et
        mass = event.m_uu
        if event.pass_only_uu and mass>130 and met>400:
            passed+=1

            eventDict = {}
            eventDict["muu"]          = mass
            eventDict["met"]          = met
            eventDict["mu_id_qop"]     = list(event.mu_id_qop)
            eventDict["mu_ms_qop"]     = list(event.mu_ms_qop)
            eventDict["mu_me_qop"]     = list(event.mu_me_qop)
            eventDict["mu_id_qoperr"]     = list(event.mu_id_qoperr)
            eventDict["mu_ms_qoperr"]     = list(event.mu_ms_qoperr)
            eventDict["mu_me_qoperr"]     = list(event.mu_me_qoperr)
            eventDict["mu_id_pt"]     = list(event.mu_id_pt)
            eventDict["mu_ms_pt"]     = list(event.mu_ms_pt)
            eventDict["mu_me_pt"]     = list(event.mu_me_pt)
            eventDict["mu_id_ptErr"]  = list(event.mu_id_ptErr)
            # eventDict["mu_id_qoperr"] = list(event.mu_id_qoperr)
            # eventDict["mu_ms_qoperr"] = list(event.mu_ms_qoperr)
            # eventDict["mu_me_qoperr"] = list(event.mu_me_qoperr)
            eventDict["mu_ms_ptErr"]  = list(event.mu_ms_ptErr)
            eventDict["mu_me_ptErr"]  = list(event.mu_me_ptErr)
            eventDict["mu_pt"]        = list(event.mu_pt)
            eventDict["mu_ptErr"]     = list(event.mu_ptErr)
            eventDict["mu_nstations"] = list(event.mu_nstations)

            eventDict["mu_eta"] = list(event.mu_eta)
            eventDict["mu_me_eta"] = list(event.mu_me_eta)
            eventDict["mu_id_eta"] = list(event.mu_id_eta)
            eventDict["mu_ms_eta"] = list(event.mu_ms_eta)

            # # adding hist for noam
            # try:
            #     truth=event.truth_Z_forTF_m/1000
            #     reco = event.m_uu
            #     if truth<2070 and reco>2070:
            #         eventDict["met_uu_migrateCrConst"] = [met,weight]
            # except:
            #     pass


            output.append(eventDict)

        if c%10000==0: 
            print c,
            sys.stdout.flush()
            # print "FINISHING EARLY";break

    # savey output
    print "Opening"
    f = open(oPath,"w")
    print "Dumptin"
    pickle.dump(output,f) 
    print "Closing"
    f.close()
    # save output to file
    print "Output saved to:", oPath
    print "#"*50

iPaths = sys.argv[1]
oDir = sys.argv[2]
monitorPath = sys.argv[3] # to be removed on completion

# iPaths = "/eos/atlas/atlascerngroupdisk/phys-exotics/lpx/zprime2018/TREE_Etienne_Jamboree/mc16_13TeV/mc16a/301569.Pythia8EvtGen_A14NNPDF23LO_DYmumu_1500M1750"
# oDir = "/afs/cern.ch/user/a/aawhite/dilepton/DileptonNTRProj/condor/output-test"
# # monitorPath = "/afs/cern.ch/user/a/aawhite/dilepton/DileptonNTRProj/condor/monitor-logBinsAdded2/job-1041.txt"

print iPaths
for i,iPath in enumerate(glob.glob(iPaths)):
    makeHist(iPath,oDir)

# remove monitor path
os.popen("rm {0}".format(monitorPath))

