#!/usr/bin/env python2
from __future__ import division
import glob, re, sys, os, math
import numpy as np
nb2fb = 1.e6

from ROOT import *

def GetXsecWeight(event,sample,applyMCwgt,nEvt_wgt,DSID):
    totWgt = 1e99
    if (applyMCwgt is False):
       totWgt = event.Nevt
    else:
       totWgt = nEvt_wgt
  
    #if event.pu_weight > 1e4 or event.pu_weight < -1e4:
    #    print event.xsec,event.geneff,totWgt
 
    if(event.xsec>0):
       return event.xsec*nb2fb * event.geneff / totWgt      # Scaling to 1ifb
    elif(DSID=="410646"):
       return 37.936*nb2fb * event.geneff / totWgt
    elif(DSID=="410647"):
       return 37.905*nb2fb * event.geneff / totWgt
    elif(DSID=="410648"):
       return 3.997*nb2fb * event.geneff / totWgt
    elif(DSID=="410649"):
       return 3.9939*nb2fb * event.geneff / totWgt
    else:
       print "ERROR: event.xsec=%g for sample=%s" % (event.xsec,sample)
       print "        quitting!"
       raise BaseException("Negative weight")
    return 1

def GetLeptonSfWeight(event,channel):
    SF = 1.
    # if(db.args.doFakes=="1" and "ee" in channel):
       # if(event.pass_only_preor_ee==1): SF = event.el_preor_SF[0]*event.el_preor_SF[1]
    if 0: pass # replace fakes call
    elif("ee" in channel and event.pass_ee==1): SF = event.el_SF[0]*event.el_SF[1]
    if("uu" in channel and event.pass_uu==1): SF = event.mu_SF[0]*event.mu_SF[1]
    if("eu" in channel and event.pass_eu==1): SF = event.el_SF[0]*event.mu_SF[0]
    if(SF<=0):
       #print "WARNING (GetLeptonSfWeight): SF=0 in channel=",channel
       SF = 1
    return SF

def GetJetSfWeight(event,channel):
    SF = 1.
    if("0b" in channel or "1b" in channel or "2b" in channel or "vbf" in channel): SF = event.btag_signal_jets_SF
    if(SF<=0):
       #print "WARNING (GetJetSfWeight): SF=0 in channel=",channel
       SF = 1
    return SF

def GetTriggerSfWeight(event,channel):
    SF = 1.
    # if(db.args.doFakes=="1" and "ee" in channel):
    #     if(event.pass_only_preor_ee==1): SF = event.trig_ee_SF
    if 0: pass # replace fakes call
    elif("ee" in channel and event.pass_ee==1): SF = event.trig_ee_SF
    if("uu" in channel and event.pass_uu==1): SF = event.trig_uu_SF
    if("eu" in channel and event.pass_eu==1): SF = event.trig_eu_SF
    if(SF<=0):
       #print "WARNING (GetTriggerSfWeight): SF=0 in channel=",channel
       SF = 1.
    return SF

def GetWeight(event,channel,sample,applyMCwgt,nEvt_wgt,DSID):
    if (sample == "data"): return 1
   
    wgt_mc = event.mconly_weight
    wgt_pu = event.pu_weight/event.mconly_weight  ## pu_weight is multiplied by mconly_weight in ST :S
    wgt_kf = event.kF_weight
    try:
        wgt_xs = GetXsecWeight(event,sample,applyMCwgt,nEvt_wgt,DSID)
    except:
        raise BaseException("Bad weight")
    wgt_lsf = GetLeptonSfWeight(event,channel)
    wgt_jsf = GetJetSfWeight(event,channel)
    wgt_tsf = GetTriggerSfWeight(event,channel)
   
    #if event.pu_weight > 1e4 or event.pu_weight < -1e4:
    #    print wgt_pu,event.pu_weight,event.mconly_weight,event.mconly_weights[0]
   
    wgt = 1.
    if applyMCwgt:
       wgt *= wgt_mc
       wgt *= wgt_pu
    wgt *= wgt_kf
    wgt *= wgt_xs
    wgt *= wgt_lsf
    #wgt *= wgt_jsf
    wgt *= wgt_tsf
    return wgt

def smearMuon(event,width):
    """ Smear muon pt in event by width % """
    m1_pt = event.mu_pt[0]
    m2_pt = event.mu_pt[1]
    m1_eta = event.mu_eta[0]
    m2_eta = event.mu_eta[1]
    m1_phi = event.mu_phi[0]
    m2_phi = event.mu_phi[1]
    m1_e = event.mu_e[0]
    m2_e = event.mu_e[1]

    # blur
    if width>0:
        blur_m1_pt = m1_pt*(1+np.random.normal(0,width,1)[0])
        blur_m2_pt = m2_pt*(1+np.random.normal(0,width,1)[0])
    else:
        blur_m1_pt = m1_pt
        blur_m2_pt = m2_pt

    # re-calc mass
    m1 = TLorentzVector()
    m1.SetPtEtaPhiE(blur_m1_pt,m1_eta,m1_phi,m1_e)
    m2 = TLorentzVector()
    m2.SetPtEtaPhiE(blur_m2_pt,m2_eta,m2_phi,m2_e)

    # re-calc energy to match muon mass
    m1.SetE(np.sqrt(m1.P()**2+0.105**2))
    m2.SetE(np.sqrt(m2.P()**2+0.105**2))

    mass = (m1+m2).Mag()
    return mass



def makeHist(iPath,oDir,i):
    """ Make histogram for one file """
    print "#"*50
    print iPath
    print i,"/",len(glob.glob(iPaths))
    print "-"*50
    # check if directory is empty
    if len(glob.glob(iPath+"/data-tinytrees/*root"))!=1:
        print "Tried to read from",iPath+"/data-tinytrees/*root"
        print "SKIPPING"
        return
    isMc = "mc16_13TeV" in iPath
    if isMc: name = ".".join(iPath.split("/")[-2:])
    else: name = os.path.dirname(iPath).split("/")[-1]
    # print name; quit()
    oPath = os.path.join(oDir,name)
    oPath+=".root"
    # print oPath; quit()
    if isMc:
        iPathHist = glob.glob(iPath+"/hist-user.*root")[0]
        # load cutflow
        histFile = TFile(iPathHist,"READ")
        cutflow = histFile.Get("cutflow_wgt_ee_pstOR")
        nEvt_wgt = cutflow.GetBinContent(1)
        DSID=re.compile("\d\d\d\d\d\d").findall(iPath)[0]
        sample = "mc"
    else:
        cutflow = 1
        nEvt_wgt = 1
        # iPathTree = glob.glob(iPath+"/*root")[0]
        DSID=1
        sample = "data"
    iPathTree = glob.glob(iPath+"/data-tinytrees/*root")[0]
    print "Loading root file:",iPathTree
    print "Loading name:","nominal"
    f = TFile.Open(iPathTree)
    try: f.GetName()
    except: print "ERROR opening file"
    events = f.Get("nominal")
    try: events.GetName()
    except: print "ERROR loading tree"

    # loop over the event
    c = 1
    hists = {}
    y0 = 0
    y1 = 10000
    yb = 200

    xb = 50
    x0 = 0
    x1 = 3.15
    # phi values (0,1)
    hists["pllmetdphi_uu"]       = TH2D("pllmetdphi_uu","pllmetdphi_uu",xb,x0,x1,yb,y0,y1)
    hists["pllmetdphi_ee"]       = TH2D("pllmetdphi_ee","pllmetdphi_ee",xb,x0,x1,yb,y0,y1)
    hists["pllmetdphi_preor_uu"] = TH2D("pllmetdphi_preor_uu","pllmetdphi_preor_uu",xb,x0,x1,yb,y0,y1)
    hists["pllmetdphi_preor_ee"] = TH2D("pllmetdphi_preor_ee","pllmetdphi_preor_ee",xb,x0,x1,yb,y0,y1)

    xb = 6000
    x0 = 0
    x1 = 6000
    hists["m_ee"]                 = TH1D("m_ee","m_ee",xb,x0,x1)
    hists["m_uu"]                 = TH1D("m_uu","m_uu",xb,x0,x1)
    hists["m_uu_smear00"]         = TH1D("m_uu_smear00","m_uu_smear00",xb,x0,x1)
    hists["m_uu_smear01"]         = TH1D("m_uu_smear01","m_uu_smear01",xb,x0,x1)
    hists["m_uu_smear02"]         = TH1D("m_uu_smear02","m_uu_smear02",xb,x0,x1)
    hists["m_uu_smear05"]         = TH1D("m_uu_smear05","m_uu_smear05",xb,x0,x1)
    hists["m_uu_smear10"]         = TH1D("m_uu_smear10","m_uu_smear10",xb,x0,x1)
    hists["m_uu_smear15"]         = TH1D("m_uu_smear15","m_uu_smear15",xb,x0,x1)
    hists["m_uu_smear20"]         = TH1D("m_uu_smear20","m_uu_smear20",xb,x0,x1)
    hists["m_uu_smear50"]         = TH1D("m_uu_smear50","m_uu_smear50",xb,x0,x1)
    hists["m_uu_smear90"]         = TH1D("m_uu_smear90","m_uu_smear90",xb,x0,x1)
    hists["m_uu_smear200"]         = TH1D("m_uu_smear200","m_uu_smear200",xb,x0,x1)
    hists["m_uu_smear1000"]         = TH1D("m_uu_smear1000","m_uu_smear1000",xb,x0,x1)
    hists["met_uu_migrate_const"]   = TH1D("met_uu_migrate_const","met_uu_migrate_const",xb,x0,x1)

    xb = 2000
    x0 = 30
    x1 = 4000
    hists["metOmll_ee"]             = TH2D("metOmll_ee","metOmll_ee",xb,x0,x1,yb,y0,y1)
    hists["metOmll_uu"]             = TH2D("metOmll_uu","metOmll_uu",xb,x0,x1,yb,y0,y1)
    # other met's
    hists["met_wmuon_cst_et"]       = TH2D("met_wmuon_cst_et","met_wmuon_cst_et",xb,x0,x1,yb,y0,y1)
    hists["met_wmuon_tst_et"]       = TH2D("met_wmuon_tst_et","met_wmuon_tst_et",xb,x0,x1,yb,y0,y1)
    hists["met_track_et"]           = TH2D("met_track_et","met_track_et",xb,x0,x1,yb,y0,y1)
    hists["met_uu"]                 = TH2D("met_uu","met_uu",xb,x0,x1,yb,y0,y1)
    hists["met_wmuon_cst_et_uu"]    = TH2D("met_wmuon_cst_et_uu","met_wmuon_cst_et_uu",xb,x0,x1,yb,y0,y1)
    hists["met_wmuon_tst_et_uu"]    = TH2D("met_wmuon_tst_et_uu","met_wmuon_tst_et_uu",xb,x0,x1,yb,y0,y1)
    hists["met_track_et_uu"]        = TH2D("met_track_et_uu","met_track_et_uu",xb,x0,x1,yb,y0,y1)
    hists["met_ee"]                 = TH2D("met_ee","met_ee",xb,x0,x1,yb,y0,y1)
    hists["met_wmuon_cst_et_ee"]    = TH2D("met_wmuon_cst_et_ee","met_wmuon_cst_et_ee",xb,x0,x1,yb,y0,y1)
    hists["met_wmuon_tst_et_ee"]    = TH2D("met_wmuon_tst_et_ee","met_wmuon_tst_et_ee",xb,x0,x1,yb,y0,y1)
    hists["met_track_et_ee"]        = TH2D("met_track_et_ee","met_track_et_ee",xb,x0,x1,yb,y0,y1)

    hists["u1MetCosTheta"]          = TH2D("u1MetCosTheta","u1MetCosTheta",xb,x0,x1,yb,y0,y1)
    hists["u2MetCosTheta"]          = TH2D("u2MetCosTheta","u2MetCosTheta",xb,x0,x1,yb,y0,y1)
    hists["e1MetCosTheta"]          = TH2D("e1MetCosTheta","e1MetCosTheta",xb,x0,x1,yb,y0,y1)
    hists["e2MetCosTheta"]          = TH2D("e2MetCosTheta","e2MetCosTheta",xb,x0,x1,yb,y0,y1)

    # log binned met
    xb=40
    arryX = np.logspace(math.log(x0,10),math.log(x1,10),xb+1)
    hists["met_uu_log"]              = TH2D("met_uu_log","met_uu_log",              xb,arryX,yb,y0,y1)
    hists["u1MetCosTheta_log"]       = TH2D("u1MetCosTheta_log","u1MetCosTheta_log",xb,arryX,yb,y0,y1)
    hists["u2MetCosTheta_log"]       = TH2D("u2MetCosTheta_log","u2MetCosTheta_log",xb,arryX,yb,y0,y1)
    hists["e1MetCosTheta_log"]       = TH2D("e1MetCosTheta_log","e1MetCosTheta_log",xb,arryX,yb,y0,y1)
    hists["e2MetCosTheta_log"]       = TH2D("e2MetCosTheta_log","e2MetCosTheta_log",xb,arryX,yb,y0,y1)

    for name in hists.keys():
        hists[name].Sumw2()

    largest = 0
    # hist = TH1D(name,name)
    passed=0
    for event in events:
        applyMCwgt =  "mc16_13TeV" in iPath
        channel = "uu"
        try: weight = GetWeight(event,channel,sample,applyMCwgt,nEvt_wgt,DSID)
        except: 
            print "Warning: skipping event"
            return
        met = event.met_wmuon_cst_et
        if event.pass_only_ee: 
            mass = event.m_ee
            if mass<130: continue
            # met = event.metOmll_ee
            hists["m_ee"].Fill(mass,weight)
            hists["metOmll_ee"].Fill(event.metOmll_ee,mass,weight)
            hists["pllmetdphi_ee"].Fill(event.pllmetdphi_ee,mass,weight)
            hists["pllmetdphi_preor_ee"].Fill(event.pllmetdphi_preor_ee,mass,weight)
            e1Theta = 2*math.atan(event.el_eta[0])
            e2Theta = 2*math.atan(event.el_eta[1])
            hists["e1MetCosTheta"].Fill(math.cos(e1Theta)*met,mass,weight)
            hists["e1MetCosTheta_log"].Fill(math.cos(e1Theta)*met,mass,weight)
            hists["e2MetCosTheta"].Fill(math.cos(e2Theta)*met,mass,weight)
            hists["e2MetCosTheta_log"].Fill(math.cos(e2Theta)*met,mass,weight)
            hists["met_wmuon_cst_et_ee"].Fill(event.met_wmuon_cst_et,mass,weight)
            hists["met_wmuon_tst_et_ee"].Fill(event.met_wmuon_tst_et,mass,weight)
            hists["met_track_et_ee"].Fill(event.met_track_et,mass,weight)
            hists["met_ee"].Fill(met,mass,weight)
        elif event.pass_only_uu:
            mass = event.m_uu
            if mass<130: continue
            # met = event.metOmll_uu
            hists["m_uu"].Fill(mass,weight)
            hists["metOmll_uu"].Fill(event.metOmll_uu,mass,weight)
            hists["pllmetdphi_uu"].Fill(event.pllmetdphi_uu,mass,weight)
            hists["pllmetdphi_preor_uu"].Fill(event.pllmetdphi_preor_uu,mass,weight)
            u1Theta = 2*math.atan(event.mu_eta[0])
            u2Theta = 2*math.atan(event.mu_eta[1])
            hists["u1MetCosTheta"].Fill(math.cos(u1Theta)*met,mass,weight)
            hists["u1MetCosTheta_log"].Fill(math.cos(u1Theta)*met,mass,weight)
            hists["u2MetCosTheta"].Fill(math.cos(u2Theta)*met,mass,weight)
            hists["u2MetCosTheta_log"].Fill(math.cos(u2Theta)*met,mass,weight)
            hists["met_wmuon_cst_et_uu"].Fill(event.met_wmuon_cst_et,mass,weight)
            hists["met_wmuon_tst_et_uu"].Fill(event.met_wmuon_tst_et,mass,weight)
            hists["met_track_et_uu"].Fill(event.met_track_et,mass,weight)
            hists["met_uu"].Fill(met,mass,weight)
            hists["met_uu_log"].Fill(met,mass,weight)
            hists["m_uu_smear00"].Fill(smearMuon(event,0.00),weight)
            hists["m_uu_smear01"].Fill(smearMuon(event,0.01),weight)
            hists["m_uu_smear02"].Fill(smearMuon(event,0.02),weight)
            hists["m_uu_smear05"].Fill(smearMuon(event,0.05),weight)
            hists["m_uu_smear10"].Fill(smearMuon(event,0.1),weight)
            hists["m_uu_smear15"].Fill(smearMuon(event,0.15),weight)
            hists["m_uu_smear20"].Fill(smearMuon(event,0.2),weight)
            hists["m_uu_smear50"].Fill(smearMuon(event,0.5),weight)
            hists["m_uu_smear90"].Fill(smearMuon(event,0.9),weight)
            hists["m_uu_smear200"].Fill(smearMuon(event,2),weight)
            hists["m_uu_smear1000"].Fill(smearMuon(event,10),weight)

            truth=event.truth_Z_forTF_m/1000
            reco = event.m_uu
            if truth<2070 and reco>2070:
                hists["met_uu_migrate_const"].Fill(met,weight)

            # # adding hist for noam
            # try:
            #     truth=event.truth_Z_forTF_m/1000
            #     reco = event.m_uu
            #     if truth<2070 and reco>2070:
            #         hists["met_uu_migrate_const"].Fill(met,weight)
            # except:
            #     pass


        if event.pass_ee or event.pass_uu:
            # largest = max(largest,event.met_wmuon_cst_et)
            # print largest; continue
            hists["met_wmuon_cst_et"].Fill(event.met_wmuon_cst_et,mass,weight)
            hists["met_wmuon_tst_et"].Fill(event.met_wmuon_tst_et,mass,weight)
            hists["met_track_et"].Fill(event.met_track_et,mass,weight)
            passed+=1


        if c%10000==0: 
            print c,
            sys.stdout.flush()
            # break
        c+=1
    print "\nLoaded",c,"events"
    print "\nPassed",c,"events"

    # savey output
    g = TFile.Open(oPath,"recreate")
    g.cd()
    for name in hists.keys():
        hists[name].Write()
    g.Write()
    g.Close()
    # save output to file
    print "Output saved to:", oPath
    print "#"*50

# basePath = "/eos/atlas/atlascerngroupdisk/phys-exotics/lpx/zprime2018/TREE_Etienne_Jamboree/"
# iPaths = basePath+"mc16_13TeV/mc16a/*"
# iPaths = basePath+"data13TeV/*/"
iPaths = sys.argv[1]
oDir = sys.argv[2]
monitorPath = sys.argv[3] # to be removed on completion

print iPaths
for i,iPath in enumerate(glob.glob(iPaths)):
    makeHist(iPath,oDir,i)

# remove monitor path
os.popen("rm {0}".format(monitorPath))

