#!/usr/bin/env python2
from __future__ import division
import glob, re, sys, os, math
import numpy as np
nb2fb = 1.e6

from ROOT import *

def GetXsecWeight(event,sample,applyMCwgt,nEvt_wgt,DSID):
    totWgt = 1e99
    if (applyMCwgt is False):
       totWgt = event.Nevt
    else:
       totWgt = nEvt_wgt
  
    #if event.pu_weight > 1e4 or event.pu_weight < -1e4:
    #    print event.xsec,event.geneff,totWgt
 
    if(event.xsec>0):
       return event.xsec*nb2fb * event.geneff / totWgt      # Scaling to 1ifb
    elif(DSID=="410646"):
       return 37.936*nb2fb * event.geneff / totWgt
    elif(DSID=="410647"):
       return 37.905*nb2fb * event.geneff / totWgt
    elif(DSID=="410648"):
       return 3.997*nb2fb * event.geneff / totWgt
    elif(DSID=="410649"):
       return 3.9939*nb2fb * event.geneff / totWgt
    else:
       print "ERROR: event.xsec=%g for sample=%s" % (event.xsec,sample)
       print "        quitting!"
       raise BaseException("Negative weight")
    return 1

def GetLeptonSfWeight(event,channel):
    SF = 1.
    # if(db.args.doFakes=="1" and "ee" in channel):
       # if(event.pass_only_preor_ee==1): SF = event.el_preor_SF[0]*event.el_preor_SF[1]
    if 0: pass # replace fakes call
    elif("ee" in channel and event.pass_ee==1): SF = event.el_SF[0]*event.el_SF[1]
    if("uu" in channel and event.pass_uu==1): SF = event.mu_SF[0]*event.mu_SF[1]
    if("eu" in channel and event.pass_eu==1): SF = event.el_SF[0]*event.mu_SF[0]
    if(SF<=0):
       #print "WARNING (GetLeptonSfWeight): SF=0 in channel=",channel
       SF = 1
    return SF

def GetJetSfWeight(event,channel):
    SF = 1.
    if("0b" in channel or "1b" in channel or "2b" in channel or "vbf" in channel): SF = event.btag_signal_jets_SF
    if(SF<=0):
       #print "WARNING (GetJetSfWeight): SF=0 in channel=",channel
       SF = 1
    return SF

def GetTriggerSfWeight(event,channel):
    SF = 1.
    # if(db.args.doFakes=="1" and "ee" in channel):
    #     if(event.pass_only_preor_ee==1): SF = event.trig_ee_SF
    if 0: pass # replace fakes call
    elif("ee" in channel and event.pass_ee==1): SF = event.trig_ee_SF
    if("uu" in channel and event.pass_uu==1): SF = event.trig_uu_SF
    if("eu" in channel and event.pass_eu==1): SF = event.trig_eu_SF
    if(SF<=0):
       #print "WARNING (GetTriggerSfWeight): SF=0 in channel=",channel
       SF = 1.
    return SF

def GetWeight(event,channel,sample,applyMCwgt,nEvt_wgt,DSID):
    if (sample == "data"): return 1
   
    wgt_mc = event.mconly_weight
    wgt_pu = event.pu_weight/event.mconly_weight  ## pu_weight is multiplied by mconly_weight in ST :S
    wgt_kf = event.kF_weight
    try:
        wgt_xs = GetXsecWeight(event,sample,applyMCwgt,nEvt_wgt,DSID)
    except:
        raise BaseException("Bad weight")
    wgt_lsf = GetLeptonSfWeight(event,channel)
    wgt_jsf = GetJetSfWeight(event,channel)
    wgt_tsf = GetTriggerSfWeight(event,channel)
   
    #if event.pu_weight > 1e4 or event.pu_weight < -1e4:
    #    print wgt_pu,event.pu_weight,event.mconly_weight,event.mconly_weights[0]
   
    wgt = 1.
    if applyMCwgt:
       wgt *= wgt_mc
       wgt *= wgt_pu
    wgt *= wgt_kf
    wgt *= wgt_xs
    wgt *= wgt_lsf
    #wgt *= wgt_jsf
    wgt *= wgt_tsf
   
    return wgt

def makeHist(iPath,oDir):
    """ Make histogram for one file """
    print "#"*50
    print iPath
    print i,"/",len(glob.glob(iPaths))
    print "-"*50
    # check if directory is empty
    if len(glob.glob(iPath+"/data-tinytrees/*root"))!=1:
        print iPath+"/data-tinytrees/*root"
        print "SKIPPING, wrong number"
        return
    isMc = "mc16_13TeV" in iPath
    if isMc: name = ".".join(iPath.split("/")[-2:])
    else: name = os.path.dirname(iPath).split("/")[-1]
    # print name; quit()
    oPath = os.path.join(oDir,name)
    oPath+=".root"
    # print oPath; quit()
    if isMc:
        iPathHist = glob.glob(iPath+"/hist-user.*root")[0]
        # load cutflow
        histFile = TFile(iPathHist,"READ")
        cutflow = histFile.Get("cutflow_wgt_ee_pstOR")
        nEvt_wgt = cutflow.GetBinContent(1)
        DSID=re.compile("\d\d\d\d\d\d").findall(iPath)[0]
        sample = "mc"
    else:
        cutflow = 1
        nEvt_wgt = 1
        # iPathTree = glob.glob(iPath+"/*root")[0]
        DSID=1
        sample = "data"
    iPathTree = glob.glob(iPath+"/data-tinytrees/*root")[0]
    print "Loading root file:",iPathTree
    print "Loading name:","nominal"
    f = TFile.Open(iPathTree)
    try: f.GetName()
    except: print "ERROR opening file"
    events = f.Get("nominal")
    try: events.GetName()
    except: print "ERROR loading tree"

    crMaxMmDest  = 1250
    srMinMmDest  = 2570
    crMaxMmConst = 2070
    srMinMmConst = 2070
    hists = {}

    hists["mmConst"] = TH1F("mmConst","mmConst",5,0,4)
    hists["mmDest"]  = TH1F("mmDest", "mmDest", 5,0,4)
    hists["mm1000"]  = TH1F("mm1000", "mm1000", 5,0,4)
    hists["mm2000"]  = TH1F("mm2000", "mm2000", 5,0,4)
    hists["mmConst_weighted"] = TH1F("mmConst_weighted","mmConst_weighted",5,0,4)
    hists["mmDest_weighted"]  = TH1F("mmDest_weighted", "mmDest_weighted", 5,0,4)
    hists["mm1000_weighted"]  = TH1F("mm1000_weighted", "mm1000_weighted", 5,0,4)
    hists["mm2000_weighted"]  = TH1F("mm2000_weighted", "mm2000_weighted", 5,0,4)

    hists["mm1to2000"]  = TH1F("mm1to000", "mm1to000", 5,0,4)
    hists["mm1to2000_weighted"]  = TH1F("mm1to000_weighted", "mm1to000_weighted", 5,0,4)

    hists["mmMassTruth"]       = TH1F("mmMassTruth", "mmMassTruth",600,0,6000)
    hists["mmMassReco"]        = TH1F("mmMassReco", "mmMassReco",600,0,6000)
    hists["mmMassTruthLoose"]  = TH1F("mmMassTruthLoose", "mmMassTruthLoose",600,0,6000)
    hists["mmMassRecoLoose"]   = TH1F("mmMassRecoLoose", "mmMassRecoLoose",600,0,6000)

    passed=0
    c=0
    for event in events:
        applyMCwgt =  "mc16_13TeV" in iPath
        channel = "uu"
        # weight=1

        try: weight = GetWeight(event,channel,sample,applyMCwgt,nEvt_wgt,DSID)
        except: 
            print "Warning: skipping event"
            return

        c+=1
        truth = event.truth_Z_forTF_m/1000
        reco  = event.m_uu

        if event.pass_uu:
            hists["mmMassTruthLoose"].Fill(truth,weight)
            hists["mmMassRecoLoose"].Fill(reco,weight)

        if event.pass_only_uu and reco>130:

            passed+=1
            # print "{0}\t{1}\t{2:.0f}".format(truth,reco,100*(reco-truth)/truth)
            # if truth<130: continue
            hists["mmMassTruth"].Fill(truth,weight)
            hists["mmMassReco"].Fill(reco,weight)

            # truth
            truthCrConst = truth<crMaxMmConst
            truthCrDest  = truth<crMaxMmDest 
            truthSrConst = truth>srMinMmConst
            truthSrDest  = truth>srMinMmDest 
            truth1000Sr  = truth>1000 
            truth2000Sr  = truth>2000 


            # reco
            recoCrConst  = reco<crMaxMmConst
            recoCrDest   = reco<crMaxMmDest 
            recoSrConst  = reco>srMinMmConst
            recoSrDest   = reco>srMinMmDest 
            reco1000Sr  = reco>1000 
            reco2000Sr  = reco>2000 

            if truthCrDest and recoCrDest:
                hists["mmDest"].Fill(0,1)
                hists["mmDest_weighted"].Fill(0,weight)
            elif truthSrDest and recoCrDest:
                hists["mmDest"].Fill(1,1)
                hists["mmDest_weighted"].Fill(1,weight)
            elif truthCrDest and recoSrDest:
                hists["mmDest"].Fill(2,1)
                hists["mmDest_weighted"].Fill(2,weight)
            elif truthSrDest and recoSrDest:
                hists["mmDest"].Fill(3,1)
                hists["mmDest_weighted"].Fill(3,weight)
            else:
                hists["mmDest"].Fill(4,1)
                hists["mmDest_weighted"].Fill(4,weight)

            if truthCrConst and recoCrConst:
                hists["mmConst"].Fill(0,1)
                hists["mmConst_weighted"].Fill(0,weight)
            elif truthSrConst and recoCrConst:
                hists["mmConst"].Fill(1,1)
                hists["mmConst_weighted"].Fill(1,weight)
            elif truthCrConst and recoSrConst:
                hists["mmConst"].Fill(2,1)
                hists["mmConst_weighted"].Fill(2,weight)
            elif truthSrConst and recoSrConst:
                hists["mmConst"].Fill(3,1)
                hists["mmConst_weighted"].Fill(3,weight)
            else:
                hists["mmConst"].Fill(4,1)
                hists["mmConst_weighted"].Fill(4,weight)


            if not truth1000Sr and not reco1000Sr:
                hists["mm1000"].Fill(0,1)
                hists["mm1000_weighted"].Fill(0,weight)
            elif truth1000Sr and not reco1000Sr:
                hists["mm1000"].Fill(1,1)
                hists["mm1000_weighted"].Fill(1,weight)
            elif not truth1000Sr and reco1000Sr:
                hists["mm1000"].Fill(2,1)
                hists["mm1000_weighted"].Fill(2,weight)
            elif truth1000Sr and reco1000Sr:
                hists["mm1000"].Fill(3,1)
                hists["mm1000_weighted"].Fill(3,weight)
            else:
                hists["mm1000"].Fill(4,1)
                hists["mm1000_weighted"].Fill(4,weight)

            if not truth2000Sr and not reco2000Sr:
                hists["mm2000"].Fill(0,1)
                hists["mm2000_weighted"].Fill(0,weight)
            elif truth2000Sr and not reco2000Sr:
                hists["mm2000"].Fill(1,1)
                hists["mm2000_weighted"].Fill(1,weight)
            elif not truth2000Sr and reco2000Sr:
                hists["mm2000"].Fill(2,1)
                hists["mm2000_weighted"].Fill(2,weight)
            elif truth2000Sr and reco2000Sr:
                hists["mm2000"].Fill(3,1)
                hists["mm2000_weighted"].Fill(3,weight)
            else:
                hists["mm2000"].Fill(4,1)
                hists["mm2000_weighted"].Fill(4,weight)

            if truth>1000:
                if not truth2000Sr and not reco2000Sr:
                    hists["mm1to2000"].Fill(0,1)
                    hists["mm1to2000_weighted"].Fill(0,weight)
                elif truth2000Sr and not reco2000Sr:
                    hists["mm1to2000"].Fill(1,1)
                    hists["mm1to2000_weighted"].Fill(1,weight)
                elif not truth2000Sr and reco2000Sr:
                    hists["mm1to2000"].Fill(2,1)
                    hists["mm1to2000_weighted"].Fill(2,weight)
                elif truth2000Sr and reco2000Sr:
                    hists["mm1to2000"].Fill(3,1)
                    hists["mm1to2000_weighted"].Fill(3,weight)
                else:
                    hists["mm1to2000"].Fill(4,1)
                    hists["mm1to2000_weighted"].Fill(4,weight)

        if c%10000==0: 
            print c,
            sys.stdout.flush()
            # break

    # savey output
    g = TFile.Open(oPath,"recreate")
    g.cd()
    for name in hists.keys():
        hists[name].Write()
    g.Write()
    g.Close()
    # save output to file
    print "Output saved to:", oPath
    print "#"*50

iPaths = sys.argv[1]
oDir = sys.argv[2]
monitorPath = sys.argv[3] # to be removed on completion

# iPaths = "/eos/atlas/atlascerngroupdisk/phys-exotics/lpx/zprime2018/TREE_Etienne_Jamboree/mc16_13TeV/mc16a/301569.Pythia8EvtGen_A14NNPDF23LO_DYmumu_1500M1750"
# oDir = "/afs/cern.ch/user/a/aawhite/dilepton/DileptonNTRProj/condor/output-test"
# # monitorPath = "/afs/cern.ch/user/a/aawhite/dilepton/DileptonNTRProj/condor/monitor-logBinsAdded2/job-1041.txt"

print iPaths
for i,iPath in enumerate(glob.glob(iPaths)):
    makeHist(iPath,oDir)

# remove monitor path
os.popen("rm {0}".format(monitorPath))

