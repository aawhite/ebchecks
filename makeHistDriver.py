import glob, re, sys, os, math, time

def header(name):
    text = ""
    text+= "Universe = vanilla\n"
    text+= "Executable = {0}/makeHist.py\n".format(workingDir)
    text+= "Output = {0}/condorOut/condorLog-{1}/$(Process).out\n".format(workingDir,name)
    text+= "Error = {0}/condorOut/condorLog-{1}/$(Process).error\n".format(workingDir,name)
    text+= "Log = {0}/condorOut/condorLog-{1}/$(Process).log\n".format(workingDir,name)
    # text+= '+JobFlavour = "workday"\n\n\n'
    return text


def entry(iPath,oDir,monitorDir,i):
    ### Monitor file stores command, is removed on successful completion of job
    monitorPath = "{0}/job-{1}.sh".format(monitorDir,i)
    cmd = "./makeHist.py {0} {1} {2} {3}\n".format(iPath,oDir,monitorPath,i)
    f = open(monitorPath,"w")
    f.write(cmd)
    f.close()
    text = ""
    text+= "# number {0}\n".format(i)
    text+= "Arguments = {0} {1} {2} {3}\n".format(iPath,oDir,monitorPath,i)
    text+= "Queue\n\n"
    # print i,
    sys.stdout.write(".")
    sys.stdout.flush()
    return text


name = "addForNoam"
basePath = "/eos/atlas/atlascerngroupdisk/phys-exotics/lpx/zprime2018/TREE_Etienne_Jamboree/"
# basePath = "/eos/atlas/atlascerngroupdisk/phys-exotics/lpx/zprime2018/TREE_Simen_Links/"
iPathsMc = basePath+"mc16_13TeV/*/*"
iPathsData = basePath+"data13TeV/*_Main/"
workingDir = "/afs/cern.ch/user/a/aawhite/dilepton/DileptonNTRProj"
monitorDir = workingDir+"/condorOut/monitor-{0}".format(name)
oDir = workingDir+"/condorOut/output-{0}".format(name)

# print "rm -r {0}; mkdir {0}".format(oDir)
# print "rm -r {0}; mkdir {0}".format(monitorDir)
os.popen("rm -r {0}; mkdir {0}".format(oDir))
os.popen("rm -r {0}; mkdir {0}".format(monitorDir))
os.popen("mkdir {0}/condorOut/condorLog-{1}/".format(workingDir,name))

jobFilePath = "job.condor"
jobFile = open(jobFilePath,"w")

jobFile.write(header(name))

for i,iPath in enumerate(glob.glob(iPathsMc)):
    jobFile.write(entry(iPath,oDir,monitorDir,i))

for j,iPath in enumerate(glob.glob(iPathsData)):
    jobFile.write(entry(iPath,oDir,monitorDir,i+j+1))
print "Total:",i+j+2

os.popen("chmod 755 {0}/*".format(monitorDir))
jobFile.close()
print ""
print '''run with\n\tcondor_submit job.condor ; repeat "condor_release aaronsw"'''
print '''condor_q aawhite | tail'''
print "Then check ls {0}".format(monitorDir)

# while 1:
#     print "#"*50
#     print os.popen("condor_q aawhite | tail").read()
#     time.sleep(10)
